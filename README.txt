MNIST
=====

Patrick Carson

This file: REAME.txt
REPO: https://Patrick_Carson@bitbucket.org/Patrick_Carson/mnist-kaggle.git

========================================================================================

Convolutional Neural Network (LeNet) approach to classifying MNIST dataset.

Used caffe for CNN implementation: https://github.com/BVLC/caffe

Placed 217 on leader board with accuracy of 98.8871%.

Leaderboard: https://www.kaggle.com/c/digit-recognizer/leaderboard